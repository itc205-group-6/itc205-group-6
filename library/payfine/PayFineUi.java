package library.payfine;
import java.util.Scanner;

public class PayFineUi {

    public static enum UiState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };

    private PayFineControl control;
    private Scanner input;
    private UiState state;

    public PayFineUi(PayFineControl control) {
        this.control = control;
        input = new Scanner(System.in);
        state = UiState.INITIALISED;
        control.setUi(this);
    }
    
    public void setState(UiState state) {
        this.state = state;
    }
    
    public void run() {
        output("Pay Fine Use Case UI\n");

        while (true) {

        switch (state) {

            case READY:
                String memberString = input("Swipe member card (press <enter> to cancel): ");
                if (memberString.length() == 0) {
                    control.cancelFine();
                    break;
                }
                try {
                    int Member_ID = Integer.valueOf(memberString).intValue();
                    control.cardSwiped(Member_ID);
                }
                catch (NumberFormatException e) {
                	output("Invalid memberId");
                }
                break;
                
            case PAYING:
                double amount = 0;
                String amountString = input("Enter amount (<Enter> cancels) : ");
                if (amountString.length() == 0) {
                    control.cancelFine();
                    break;
                }
                try {
                    amount = Double.valueOf(amountString).doubleValue();
                }
                catch (NumberFormatException e) {}
                if (amount <= 0) {
                    output("Amount must be positive");
                    break;
                }
                control.payFine(amount);
                break;

            case CANCELLED:
                output("Pay Fine process cancelled");
                return;

            case COMPLETED:
                output("Pay Fine process complete");
                return;

            default:
                output("Unhandled state");
                throw new RuntimeException("FixBookUI : unhandled state :" + state);			

            }
        }
    }
    
    private String input(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }

    private void output(Object object) {
        System.out.println(object);
    }

    public void display(Object object) {
        output(object);
    }

}
