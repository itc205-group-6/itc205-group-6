package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

    private ReturnBookUi returnBookUi;
    private enum ControlState { INITIALISED, READY, INSPECTING };
    private ControlState state;
    private Library library;
    private Loan currentLoan;
    
    public ReturnBookControl() {
        this.library = Library.getInstance();
        state = ControlState.INITIALISED;
    }
    
    public void setUi(ReturnBookUi ui) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
        }
        this.returnBookUi = ui;
        ui.setState(ReturnBookUi.UiState.READY);
        state = ControlState.READY;
    }

    public void bookScanned(int bookId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
        }
        Book currentBook = library.getBook(bookId);
        
        if (currentBook == null) {
            returnBookUi.display("Invalid Book Id");
            return;
        }
        if (!currentBook.isOnLoan()) {
            returnBookUi.display("Book has not been borrowed");
            return;
        }
        currentLoan = library.getLoanByBookId(bookId);
        double overdueFine = 0.0;
        if (currentLoan.isOverdue()) {
            overdueFine = library.calculateOverdueFine(currentLoan);
        }
        String currentBookString = currentBook.toString();
        String currentLoanString = currentLoan.toString();
        returnBookUi.display("Inspecting");
        returnBookUi.display(currentBookString);
        returnBookUi.display(currentLoanString);

        if (currentLoan.isOverdue()) {
            String formattedOverdueFine = String.format("\nOverdue fine : $%.2f", overdueFine);
            returnBookUi.display(formattedOverdueFine);
        }
        returnBookUi.setState(ReturnBookUi.UiState.INSPECTING);
        state = ControlState.INSPECTING;
    }

    public void scanningComplete() {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
        }
        returnBookUi.setState(ReturnBookUi.UiState.COMPLETED);
    }

    public void dischargeLoan(boolean isDamaged) {
        if (!state.equals(ControlState.INSPECTING)) {
            throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
        }
        library.dischargeLoan(currentLoan, isDamaged);
        currentLoan = null;
        returnBookUi.setState(ReturnBookUi.UiState.READY);
        state = ControlState.READY;
    }
}
