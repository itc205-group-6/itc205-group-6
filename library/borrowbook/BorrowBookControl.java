package library.borrowbook;
import java.util.ArrayList;
import java.util.List;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {
	
    private BorrowBookUi borrowBookUi;
    private Library library;
    private Member member;
    private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
    private ControlState state;
    private List<Book> pendingList;
    private List<Loan> completedList;
    private Book book;
	
    public BorrowBookControl() {
        this.library = Library.getInstance();
        state = ControlState.INITIALISED;
    }

    public void setUi(BorrowBookUi borrowBookUi) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
        }
        this.borrowBookUi = borrowBookUi;
        borrowBookUi.setState(BorrowBookUi.UiState.READY);
        state = ControlState.READY;		
    }

    public void swiped(int memberId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
        }
        member = library.getMember(memberId);
        if (member == null) {
            borrowBookUi.display("Invalid memberId");
            return;
        }
        if (library.canMemberBorrow(member)) {
            pendingList = new ArrayList<>();
            borrowBookUi.setState(BorrowBookUi.UiState.SCANNING);
            state = ControlState.SCANNING; 
        }
        else {
            borrowBookUi.display("Member cannot borrow at this time");
            borrowBookUi.setState(BorrowBookUi.UiState.RESTRICTED); 
        }
    }

    public void scanned(int bookId) {
        book = null;
        if (!state.equals(ControlState.SCANNING)) {
            throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
        }
        book = library.getBook(bookId);
        if (book == null) {
            borrowBookUi.display("Invalid bookId");
            return;
        }
        if (!book.isAvailable()) {
            borrowBookUi.display("Book cannot be borrowed");
            return;
        }
        pendingList.add(book);
        for (Book book : pendingList) {
            borrowBookUi.display(book.toString());
        }
        if (library.getNumberOfLoansRemainingForMember(member) - pendingList.size() == 0) {
            borrowBookUi.display("Loan limit reached");
            complete();
        }
    }

    public void complete() {
        if (pendingList.size() == 0) {
            cancel();
        }
        else {
            borrowBookUi.display("\nFinal Borrowing List");
        for (Book book : pendingList) {
            borrowBookUi.display(book.toString());
        }
            completedList = new ArrayList<Loan>();
            borrowBookUi.setState(BorrowBookUi.UiState.FINALISING);
            state = ControlState.FINALISING;
        }
    }

    public void commitLoans() {
        if (!state.equals(ControlState.FINALISING)) {
                throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
        }
        for (Book book : pendingList) {
            Loan loan = library.issueLoan(book, member);
            completedList.add(loan);			
        }
        borrowBookUi.display("Completed Loan Slip");
        for (Loan loan : completedList) {
            borrowBookUi.display(loan.toString());
        }
        borrowBookUi.setState(BorrowBookUi.UiState.COMPLETED);
        state = ControlState.COMPLETED;
    }

    public void cancel() {
        borrowBookUi.setState(BorrowBookUi.UiState.CANCELLED);
        state = ControlState.CANCELLED;
    }		
}