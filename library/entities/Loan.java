package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
    
    public static enum LoanState { CURRENT, OVER_DUE, DISCHARGED };
    
    private int loanId;
    private Book loanedBook;
    private Member loaningMember;
    private Date loanDueDate;
    private LoanState loanCurrentState;
    
    public Loan(int loanId, Book book, Member member, Date dueDate) {
        this.loanId = loanId;
        this.loanedBook = book;
        this.loaningMember = member;
        this.loanDueDate = dueDate;
        this.loanCurrentState = LoanState.CURRENT;
    }
    
    public void checkOverdue() {
        if (loanCurrentState == LoanState.CURRENT && Calendar.getInstance().getDate().after(loanDueDate)) {
            this.loanCurrentState = LoanState.OVER_DUE;
        }
    }
    
    public boolean isOverdue() {
        return loanCurrentState == LoanState.OVER_DUE;
    }
    
    public Integer getId() {
        return loanId;
    }

    public Date getDueDate() {
        return loanDueDate;
    }

    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        StringBuilder loanStringBuilder = new StringBuilder();


        int memberId = loaningMember.getId();
        String memberLastName = loaningMember.getLastName();
        String memberFirstName = loaningMember.getFirstName();
        int bookId = loanedBook.getId();
        String bookTitle = loanedBook.getTitle();
        String dueDate = simpleDateFormat.format(loanDueDate);

        loanStringBuilder.append("Loan:  ").append(loanId).append("\n")
            .append("  Borrower ").append(memberId).append(" : ")
            .append(memberLastName).append(", ").append(memberFirstName).append("\n")
            .append("  Book ").append(bookId).append(" : " )
            .append(bookTitle).append("\n")
            .append("  DueDate: ").append(dueDate).append("\n")
            .append("  State: ").append(loanCurrentState);
        
        return loanStringBuilder.toString();
    }

    public Member getMember() {
        return loaningMember;
    }

	
    public Book getBook() {
        return loanedBook;
    }


    public void discharge() {
        loanCurrentState = LoanState.DISCHARGED;
    }
}
