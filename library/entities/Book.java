package library.entities;
import java.io.Serializable;


@SuppressWarnings("serial")
public class Book implements Serializable {
    
    private enum BookState { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };

    private String bookTitle;
    private String bookAuthor;
    private String bookCallNumber;
    private int bookId;
    private BookState bookCurrentState;
    
    public Book(String author, String title, String callNo, int id) {
        this.bookAuthor = author;
        this.bookTitle = title;
        this.bookCallNumber = callNo;
        this.bookId = id;
        this.bookCurrentState = BookState.AVAILABLE;
    }
    
    public String toString() {
        StringBuilder bookStringBuilder = new StringBuilder();
        bookStringBuilder.append("Book: ").append(bookId).append("\n")
            .append("  Title:  ").append(bookTitle).append("\n")
            .append("  Author: ").append(bookAuthor).append("\n")
            .append("  CallNo: ").append(bookCallNumber).append("\n")
            .append("  State:  ").append(bookCurrentState);
        
        return bookStringBuilder.toString();
    }

    public Integer getId() {
        return bookId;
    }

    public String getTitle() {
        return bookTitle;
    }
    
    public boolean isAvailable() {
        return bookCurrentState == BookState.AVAILABLE;
    }
    
    public boolean isOnLoan() {
        return bookCurrentState == BookState.ON_LOAN;
    }
    
    public boolean isDamaged() {
        return bookCurrentState == BookState.DAMAGED;
    }
    
    public void borrowBook() {
        if (bookCurrentState.equals(BookState.AVAILABLE)) {
            bookCurrentState = BookState.ON_LOAN;
        }
        else {
            String exception = String.format("Book: cannot borrow while book is in state: %s", bookCurrentState);
            throw new RuntimeException(exception);
        }
    }

    public void returnBook(boolean damaged) {
        if (bookCurrentState.equals(BookState.ON_LOAN)) {
            if (damaged) {
                bookCurrentState = BookState.DAMAGED;
            }
            else {
                bookCurrentState = BookState.AVAILABLE;
            }
        }
        else {
            String exception = String.format("Book: cannot Return while book is in state: %s", bookCurrentState);
            throw new RuntimeException(exception);
        }
    }
    
    public void repairBook() {
        if (bookCurrentState.equals(BookState.DAMAGED)) {
            bookCurrentState = BookState.AVAILABLE;
        }
        else {
            String exception = String.format("Book: cannot repair while book is in state: %s", bookCurrentState);
            throw new RuntimeException(exception);
        }
    }
}
